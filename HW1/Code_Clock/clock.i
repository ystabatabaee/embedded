
#pragma used+
sfrb TWBR=0;
sfrb TWSR=1;
sfrb TWAR=2;
sfrb TWDR=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      
sfrb ADCSRA=6;
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRRL=9;
sfrb UCSRB=0xa;
sfrb UCSRA=0xb;
sfrb UDR=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   
sfrb UBRRH=0x20;
sfrb UCSRC=0X20;
sfrb WDTCR=0x21;
sfrb ASSR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb SFIOR=0x30;
sfrb OSCCAL=0x31;
sfrb OCDR=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TWCR=0x36;
sfrb SPMCR=0x37;
sfrb TIFR=0x38;
sfrb TIMSK=0x39;
sfrb GIFR=0x3a;
sfrb GICR=0x3b;
sfrb OCR0=0X3c;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x40
	.EQU __sm_mask=0xB0
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0xA0
	.EQU __sm_ext_standby=0xB0
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#endasm

void _lcd_write_data(unsigned char data);

unsigned char lcd_read_byte(unsigned char addr);

void lcd_write_byte(unsigned char addr, unsigned char data);

void lcd_gotoxy(unsigned char x, unsigned char y);

void lcd_clear(void);
void lcd_putchar(char c);

void lcd_puts(char *str);

void lcd_putsf(char flash *str);

void lcd_putse(char eeprom *str);

void lcd_init(unsigned char lcd_columns);

#pragma library alcd.lib

typedef char *va_list;

#pragma used+

char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
int printf(char flash *fmtstr,...);
int sprintf(char *str, char flash *fmtstr,...);
int vprintf(char flash * fmtstr, va_list argptr);
int vsprintf(char *str, char flash * fmtstr, va_list argptr);

char *gets(char *str,unsigned int len);
int snprintf(char *str, unsigned int size, char flash *fmtstr,...);
int vsnprintf(char *str, unsigned int size, char flash * fmtstr, va_list argptr);

int scanf(char flash *fmtstr,...);
int sscanf(char *str, char flash *fmtstr,...);

#pragma used-

#pragma library stdio.lib

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

int sectenth = 0, second = 0, minute = 0, hour = 0;
int sw_sectenth = 0, sw_second = 0, sw_minute = 0, sw_hour = 0;
int al_second = 0, al_minute = 0, al_hour = 0;
int sw_flag = 0, alarm_flag = 0;
int state = 0;

char msg[32], time_str[32];

interrupt [20] void timer0_comp_isr(void)
{
if (sw_flag) {  
sw_sectenth ++;
if (sw_sectenth >= 10)  { 
sw_sectenth = 0;
sw_second ++;
if (sw_second >= 60)  {  
sw_second = 0;
sw_minute ++; 
if (sw_minute >= 60)  {  
sw_minute = 0;
sw_hour ++;
if (sw_hour >= 24)  {  
sw_hour = 0;
} 
}    
}  
sprintf(time_str, "Stop Watch  %02d:%02d:%02d       ", sw_hour, sw_minute, sw_second);
}
}
sectenth ++;
if (sectenth >= 10)  { 
sectenth = 0;
second ++;
if (second >= 60)  {  
second = 0;
minute ++; 
if (minute >= 60)  {  
minute = 0;
hour ++;
if (hour >= 24)  {  
hour = 0;
} 
}    
} 
if (state != 8 && !alarm_flag){ 
if (second == al_second && minute == al_minute && hour == al_hour) 
sprintf(time_str, "Time  %02d:%02d:%02d   **ALARM**", hour, minute, second);
else
sprintf(time_str, "Time  %02d:%02d:%02d             ", hour, minute, second);    
}  
if (alarm_flag) {
sprintf(time_str, "Alarm  %02d:%02d:%02d            ", al_hour, al_minute, al_second);
}  

}   
}

interrupt [2] void ext_int0_isr(void)
{
switch(state) {
case 0:  
lcd_clear(); 
sprintf(msg, "B2. Set Second -- B1. Minute"); 
state = 1;  
break;  

case 1:
lcd_clear();
sprintf(msg, "B2. Set Minute -- B1. Hour"); 
state = 2;  
break;

case 2:
lcd_clear();
sprintf(msg, "B2. Set Hour -- B1. Home"); 
state = 3;
break;

case 3:
sprintf(msg, "B1. Set Time -- B2. SW/Alarm");
state =  0; 
lcd_clear();
delay_ms(5);
break;

case 4: 
state =  5; 
alarm_flag = 1;
sprintf(msg, "B2. Set Second -- B1. Minute");
lcd_clear();
delay_ms(5);
break;

case 5:
state = 6;
sprintf(msg, "B2. Set Minute -- B1. Hour"); 
lcd_clear();
break;

case 6:
state = 7;
sprintf(msg, "B2. Set Hour -- B1. Home");
lcd_clear();
break; 

case 7:
sprintf(msg, "B1. Set Time -- B2. SW/Alarm"); 
alarm_flag = 0;
state =  0;  
lcd_clear(); 
break; 

case 8: 
sprintf(msg, "B1. Set Time -- B2. SW/Alarm");
state = 0; 
sw_flag = 0;
lcd_clear();
break;                                                         
}
}

interrupt [3] void ext_int1_isr(void)
{
switch(state) {
case 0:  
lcd_clear();
state =  4;
sprintf(msg, "B1. Set Alarm -- B2. Stop Watch");  
break;  

case 1: 
second ++;
if (second >= 60)
second = 0;
state =  1; 
break;

case 2: 
minute ++;
if (minute >= 60)
minute = 0;
state =  2;
break;

case 3:
hour ++;
if (hour >= 24)
hour = 0;
state =  3; 
break;

case 4: 
state =  8;
sw_sectenth = 0;
sw_second = 0;
sw_minute = 0;
sw_hour = 0;
sw_flag = 1; 
lcd_clear(); 
sprintf(msg, "B2. Stop/Start -- B1. Home");
break;

case 5:
al_second ++;
if (al_second >= 60)
al_second = 0;
state =  5;
break;

case 6:
al_minute ++;
if (al_minute >= 60)
al_minute = 0;
state =  6;
break; 

case 7:
al_hour ++;
if (al_hour >= 24)
al_hour = 0;
state =  7;
break; 

case 8:
sw_flag = 1 - sw_flag;
state =  8;
break;                                                         
}
}

void main(void)
{    
PORTD = 0x0C;
DDRD = 0x00;

TCCR0=(0<<6       ) | (0<<5       ) | (0<<4       ) | (1<<3       ) | (1<<2       ) | (0<<1       ) | (1<<0       );
TCNT0=0x00;
OCR0=0x60;

TIMSK=(1<<7       ) | (0<<6       ) | (0<<5       ) | (0<<4       ) | (0<<3       ) | (0<<2       ) | (1<<1       ) | (0<<0       );

GICR|=0xc0;
MCUCR=0x0a;
MCUCSR= 0x00; 
GIFR = 0xc0;

lcd_init(32);
#asm("sei")
sprintf(msg, "B1. Set Time -- B2. SW/Alarm");

while (1)
{ 
lcd_gotoxy(0, 0);    
lcd_puts(msg); 
lcd_gotoxy(0, 1);    
lcd_puts(time_str);
delay_ms(500); 
} 

}
