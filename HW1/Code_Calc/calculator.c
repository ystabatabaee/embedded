#include <mega16.h>
#include <alcd.h>
#include <delay.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

// states
#define DEFAULT 0
#define NUMBER 1
#define ADVANCED 2
#define GEO 5
#define PRIMARY 3
#define RESULT 4

#define ERROR 123456789

char keypad(void);
void fsm(void);
float evaluate(void);
int get_last_num(int);
int get_next_num(int, int);


flash char shift[4] = {0x77, 0x6F, 0x5F, 0x3F};
flash char layout[12] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#'};
char msg[32];
char expression[32];
float val[32]; 
char types[32]; 
int state = DEFAULT;
int num;
int exp_len;
int num_flag = 0;


void main(void)
{
    int i;
    PORTD = 0x7F;
    DDRD = 0x78;
    lcd_init(32);  
    sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
    exp_len = 0; 
    sprintf(expression, "Expr: ");
    for (i = 0; i < 32; i ++){ 
        val[i] = 0.0;
        types[i] = 'x';
    } 
     
    while (1)
    {   
        lcd_clear();
        lcd_gotoxy(0, 0); 
        lcd_puts(msg);
        lcd_gotoxy(0, 1);
        lcd_puts(expression); 
        fsm();        
    }
}

void fsm(void)
{
    char ch = keypad();
    char expr[32];
    char res[10]; 
    float result = 0.0; 
    
    if (ch == '#'){
        if (num_flag == 1){ 
            val[exp_len] = num;
            num_flag = 0; 
            types[exp_len] = 'N';
            exp_len ++;
            num = 0;                
        } 
        state = RESULT;
        result = evaluate();
        if (result == ERROR){ 
            sprintf(res, "Error");
        } else{
            ftoa(result, 2, res);
        }    
        sprintf(msg, "Enter * to clear."); 
        sprintf(expr, "%s=%s", expression, res);
        strcpy(expression, expr); 
        return;         
    }  
    
    switch(state){  
        case DEFAULT:
            switch(ch){
                case '0':
                    state = NUMBER; 
                    sprintf(msg, "Enter A Digit: 0-9"); 
                    break;
                case '1':
                    state = PRIMARY; 
                    sprintf(msg, "1:+  2:-  3:/  4:*  5:^ 6:( 7:)");
                    break;
                case '2':
                    state = ADVANCED;
                    sprintf(msg, "1:sqrt  2:exp  3:log  4:neg"); 
                    break; 
                case '3':
                    state = GEO;
                    sprintf(msg, "1:sin  2:cos  3:tan"); 
                    break;                   
            }
            break;
        
        case NUMBER:
            if (ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7' || ch == '8' || ch == '9'){ 
                num_flag = 1;
                num = num * 10 + (int)(ch - '0');
                state =  DEFAULT; 
                sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo"); 
                sprintf(expr, "%s%c", expression, ch);
                strcpy(expression, expr); 
            }  
            break; 
            
        case ADVANCED:
            if (num_flag == 1){ 
                num_flag = 0;
                val[exp_len] = num; 
                types[exp_len] = 'N';
                exp_len ++;
                num = 0;                
            }
             
            if (ch == '1' || ch == '2' || ch == '3' || ch == '4'){ 
                state =  DEFAULT; 
                sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo"); 
                if (ch == '1'){ 
                    sprintf(expr, "%ssqrt", expression);
                    types[exp_len] = 'S';
                }    
                if (ch == '2') {
                    sprintf(expr, "%sexp", expression); 
                    types[exp_len] = 'e';
                }    
                if (ch == '3') {
                    sprintf(expr, "%slog", expression); 
                    types[exp_len] = 'l';
                }    
                if (ch == '4') { 
                    sprintf(expr, "%sneg", expression);
                    types[exp_len] = 'n';
                }    
                strcpy(expression, expr);
                exp_len ++;                     
            }  
            break; 
            
        case PRIMARY:
            if (num_flag == 1){ 
                val[exp_len] = num;
                num_flag = 0; 
                types[exp_len] = 'N';
                exp_len ++;
                num = 0;                
            }
            
            if (ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7'){ 
                state =  DEFAULT; 
                sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo"); 
                if (ch == '1'){
                    sprintf(expr, "%s+", expression);
                    types[exp_len] = '+'; 
                }   
                if (ch == '2') {  
                    sprintf(expr, "%s-", expression); 
                    types[exp_len] = '-'; 
                }    
                if (ch == '3') {
                    sprintf(expr, "%s/", expression); 
                    types[exp_len] = '/'; 
                }
                if (ch == '4') {
                    sprintf(expr, "%s*", expression); 
                    types[exp_len] = '*'; 
                 }   
                if (ch == '5') { 
                    sprintf(expr, "%s^", expression); 
                    types[exp_len] = '^';
                }
                if (ch == '6') {
                    sprintf(expr, "%s(", expression);  
                    types[exp_len] = '(';
                 }   
                if (ch == '7')  {
                    sprintf(expr, "%s)", expression); 
                    types[exp_len] = ')';
                 }   
                strcpy(expression, expr); 
                exp_len ++;                    
            }
            break;
            
        case GEO:
           if (num_flag == 1){ 
                num_flag = 0;
                val[exp_len] = num; 
                types[exp_len] = 'N';
                exp_len ++;
                num = 0;                
            }
            if (ch == '1' || ch == '2' || ch == '3'){ 
                state =  DEFAULT; 
                sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo"); 
                if (ch == '1') {
                    sprintf(expr, "%ssin", expression);   
                    types[exp_len] = 's';
                }    
                if (ch == '2') {
                    sprintf(expr, "%scos", expression);  
                    types[exp_len] = 'c';
                }    
                if (ch == '3') {
                    sprintf(expr, "%stan", expression);  
                    types[exp_len] = 't';
                }    
                strcpy(expression, expr);
                exp_len ++;                   
            }
            break;    
            
        case RESULT: 
            if (ch == '*'){  
                int i = 0;
                state =  DEFAULT; 
                sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo"); 
                sprintf(expression, "Expr: ");
                exp_len = 0; 
                for (i = 0; i < 32; i ++){ 
                    val[i] = 0.0;
                    types[i] = 'x';
                }
            }
            break;              
    }
    
}

float evaluate(void)
{
    int i, start, end; 
    int idx1, idx2; 
    int token_count = exp_len; 
    int paren_flag = 0; 

    while(token_count > 1)
    {  
       i = 0;
       start = -1;
       end = exp_len; 
       while(i < end)
        { 
            if (types[i] == '('){ 
                paren_flag = 1;
                start = i;
            }  
            if (types[i] == ')'){
                paren_flag = 0; 
                end = i; 
                if (start != -1){ 
                    types[start] = 'x';
                    types[i] = 'x';
                    token_count -= 2;
                }
                else{ 
                    return ERROR;
                }
                break;
            } 
            i ++;
        }
        
        if (paren_flag == 1)
            return ERROR;  
        
        // neg
        for (i = start + 1; i < end; i ++)
        {              
            if (types[i] == 'n'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = -val[idx2]; 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }     
        }               
        
        
        // sin, cos, tan
        for (i = start + 1; i < end; i ++)
        {              
            if (types[i] == 's'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = sin(val[idx2]); 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }
            
            if (types[i] == 'c'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = cos(val[idx2]); 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }
            
            if (types[i] == 't'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = tan(val[idx2]); 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }             
        }  
        
        // exp, log
        for (i = start + 1; i < end; i ++)
        {  
            if (types[i] == 'e'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = exp(val[idx2]); 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }
            
            if (types[i] == 'l'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = log(val[idx2]); 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }             
        }
        
        // pow and sqrt
        for (i = start + 1; i < end; i ++)
        {  
            if (types[i] == '^'){ 
                idx1 = get_last_num(i);
                idx2 = get_next_num(i, end);
                if (idx1 == -1 || idx2 == -1) return ERROR; 
                val[i] = pow(val[idx1], val[idx2]); 
                types[idx1] = 'x';
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 2;          
            }
            
            if (types[i] == 'S'){ 
                idx2 = get_next_num(i, end); 
                if (idx2 == -1) return ERROR;
                val[i] = sqrt(val[idx2]); 
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 1;          
            }    
        } 
          
        
        // * and /
        for (i = start + 1; i < end; i ++)
        {  
            if (types[i] == '*'){ 
                idx1 = get_last_num(i);
                idx2 = get_next_num(i, end); 
                if (idx1 == -1 || idx2 == -1) return ERROR;
                val[i] = val[idx1] * val[idx2]; 
                types[idx1] = 'x';
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 2;          
            }
            
            if (types[i] == '/'){ 
                idx1 = get_last_num(i);
                idx2 = get_next_num(i, end);
                if (idx1 == -1 || idx2 == -1) return ERROR; 
                val[i] = val[idx1] / val[idx2]; 
                types[idx1] = 'x';
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 2;          
            }             
        }
         
        // + and -
        for (i = start + 1; i < end; i ++)
        {  
            if (types[i] == '+'){ 
                idx1 = get_last_num(i);
                idx2 = get_next_num(i, end); 
                if (idx1 == -1 || idx2 == -1) return ERROR;
                val[i] = val[idx1] + val[idx2]; 
                types[idx1] = 'x';
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 2;          
            }
            
            if (types[i] == '-'){ 
                idx1 = get_last_num(i);
                idx2 = get_next_num(i, end);
                if (idx1 == -1 || idx2 == -1) return ERROR; 
                val[i] = val[idx1] - val[idx2]; 
                types[idx1] = 'x';
                types[idx2] = 'x';
                types[i] = 'N';  
                token_count -= 2;          
            }             
        } 
    }
    for (i = 0; i < exp_len; i ++)
    {  
         if (types[i] == 'N'){ 
            return val[i];         
         }             
    }
     
    return val[0]; 

}

int get_last_num(int index)
{
    int i = 0;
    for (i = index - 1 ; i >= 0; i --)
    {  
        if (types[i] == 'N'){ 
            return  i;
        } else if (types[i] != 'x'){ 
            return -1;
        }              
    }  
    return -1;  
}


int get_next_num(int index, int end)
{
    int i = 0;
    for (i = index + 1; i < end; i ++)
    {  
        if (types[i] == 'N'){ 
            return  i;
        }  else if (types[i] != 'x'){ 
            return -1;
        }             
    }  
    return -1;  
}

char keypad(void)
{
    int row = 0, column = -1, position = 0; 
    while(1){
        for (row = 0; row < 4; row ++){  
            PORTD = shift[row];  
            if (PIND.2 == 0) { 
                column = 0;
            } 
            if (PIND.1 == 0) { 
                column = 1;
            }
            if (PIND.0 == 0) { 
                column = 2;
            }
            if (column != -1) { 
                position = row * 3  + column;
                column = -1;
                while(PIND.2 == 0);
                while(PIND.1 == 0);
                while(PIND.0 == 0);
                return layout[position]; 
            } 
            delay_ms(5); 
        }
    } 
}
