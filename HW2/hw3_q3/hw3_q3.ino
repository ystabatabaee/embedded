#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// inputs, outputs
const int start_but = 10;
const int stop_but = 13;
const int memory_but = A0;
const int next_but = A1;
const int edit_but = A2;
const int new_but = A3;
const int delete_but = A4;
const int door_but = 6;
const int lamp = 7;
const int heater = 8;
const int beep = 9;

char time_str[10];
char msg[16];
int saved_times[32];

int time_sec = 0;
long start_millis = 0;
int saved_times_cnt = 0;
int saved_times_ptr = 0;

// states
enum state {
  IDLE,
  HEATER,
  PENDING,
  PENDING_OPEN,
  READY,
  CHOOSE_TIME,
  SET_SEC,
  SET_MIN
};

state next_state = IDLE;
state prev_state = IDLE;


void setup() {
  lcd.begin(16, 2);
  lcd.print("00:00  ");
  sprintf(msg, "Idle!      ");
  
  pinMode(start_but, INPUT_PULLUP);
  pinMode(stop_but, INPUT_PULLUP);
  pinMode(memory_but, INPUT);
  pinMode(next_but, INPUT);
  pinMode(edit_but, INPUT);
  pinMode(new_but, INPUT);
  pinMode(delete_but, INPUT);
  pinMode(door_but, INPUT);
  
  pinMode(lamp, OUTPUT);
  pinMode(heater, OUTPUT);
  pinMode(beep, OUTPUT);   
}

void loop() {
  int start = digitalRead(start_but);
  int stop = digitalRead(stop_but);
  int memory = digitalRead(memory_but);
  int next = digitalRead(next_but);
  int edit = digitalRead(edit_but); 
  int new_ = digitalRead(new_but);
  int delete_ = digitalRead(delete_but);
  int door = digitalRead(door_but);
  
  lcd.setCursor(0, 0);
  lcd.print(time_str);
  lcd.setCursor(0, 1);
  lcd.print(msg);
  
  switch (prev_state) {
    case IDLE:
      sprintf(time_str, "%02d:%02d  ", time_sec / 60, time_sec % 60);
      sprintf(msg, "Idle!         ");
      if (start  == 1){
        start_millis = millis();
        next_state = HEATER;
        time_sec = 30;
      }
    	
      if (memory == 1){
        next_state = CHOOSE_TIME;
      }
      
      break;
    
    case HEATER:
        sprintf(time_str, "%02d:%02d  ", time_sec / 60, time_sec % 60);
        sprintf(msg, "Heating...    ");
    	digitalWrite(heater, HIGH);
    	if (time_sec == 0){
          next_state = READY;
          break;   
        }
    	if (millis() - start_millis >= 1000){
          time_sec -= 1;
		  start_millis = millis();
        }
    	if (start  == 1){
          next_state = HEATER;
          time_sec += 30;
      	}
        if (stop == 1){
          next_state = PENDING;
        }
    	if (door == 1){
          next_state = PENDING_OPEN;
        }
    	break; 
    
    case PENDING:
        sprintf(time_str, "%02d:%02d  ", time_sec / 60, time_sec % 60);
    	sprintf(msg, "Heating stoped");
    	digitalWrite(heater, LOW);
    	digitalWrite(lamp, LOW);
    	if (stop == 1){
          time_sec = 0;
          next_state = IDLE;
        }
    	if (start == 1){
          next_state = HEATER;
        }
    	break;
    
    case PENDING_OPEN:
        sprintf(time_str, "%02d:%02d  ", time_sec / 60, time_sec % 60);
    	sprintf(msg, "Door opened   ");
    	digitalWrite(lamp, HIGH);
    	digitalWrite(heater, LOW);
    	if (door == 1){
          next_state = PENDING;
        }
    	break;
    
    case READY:
        sprintf(time_str, "%02d:%02d  ", time_sec / 60, time_sec % 60);
    	sprintf(msg, "Food is Ready!");
    	digitalWrite(lamp, LOW);
    	tone(beep, 300, 300);
    	if (start  == 1){
          noTone(beep);
          start_millis = millis();
          next_state = HEATER;
          time_sec = 30;
        }
    	if (door == 1){
          noTone(beep);
          next_state = IDLE;
        }
    	break;
    
    case CHOOSE_TIME:  
        sprintf(msg, "Select Time   ");
        if (saved_times_cnt == 0){
          sprintf(time_str, "No Time");
        } else {
          sprintf(time_str, "%d-%02d:%02d  ", saved_times_ptr + 1, saved_times[saved_times_ptr] / 60, saved_times[saved_times_ptr] % 60);
        }
        if (next == 1) {
          if (saved_times_ptr < saved_times_cnt - 1){
            saved_times_ptr += 1;
          } else {
            saved_times_ptr = 0;
          } 
        }
        if (edit == 1) {
          next_state = SET_SEC; 
        }
        if (new_ == 1) {
          saved_times_cnt += 1;
          saved_times_ptr = saved_times_cnt - 1; 
          saved_times[saved_times_ptr] = 0;
          next_state = SET_SEC;
        }
    	if (delete_ == 1) {
          for (int i = saved_times_ptr; i < saved_times_cnt - 1; i ++){
            saved_times[i] = saved_times[i + 1];
          }
          if (saved_times_ptr != 0){
            saved_times_ptr -= 1;
          }
          saved_times_cnt -= 1;
        }
    	if (memory == 1){
          next_state = IDLE;
        }
        if (start == 1){
          start_millis = millis();
          next_state = HEATER;
          time_sec = saved_times[saved_times_ptr];
      	}
    	break;
    
    case SET_SEC:
    	sprintf(msg, "Set Second   ");
    	sprintf(time_str, "%d-%02d:%02d  ", saved_times_ptr + 1, saved_times[saved_times_ptr] / 60, saved_times[saved_times_ptr] % 60);
    	if (next == 1) {
          saved_times[saved_times_ptr] += 1;
        }
        if (edit == 1) {
          next_state = SET_MIN;
        }
    	break;
    
    case SET_MIN:
    	sprintf(msg, "Set Minute   ");
    	sprintf(time_str, "%d-%02d:%02d  ", saved_times_ptr + 1, saved_times[saved_times_ptr] / 60, saved_times[saved_times_ptr] % 60);
    	if (next == 1) {
          saved_times[saved_times_ptr] += 60;
        }
    	if (edit == 1) {
          next_state = CHOOSE_TIME;
        }
    	break;
  }
  prev_state = next_state;

  
  delay(200);
}